<?php
const FILE_INPUT = 'input.txt';
const FILE_OUTPUT = 'output.txt';


if (file_exists(FILE_INPUT)){
    $text = file_get_contents(FILE_INPUT);
    $text = modifyText($text);
    file_put_contents(FILE_OUTPUT, $text);
} else {
    echo "File ".FILE_INPUT." not found!";
}


function modifyText($text){
    $words = preg_split('/([^\pL\']+)/u', $text, -1, PREG_SPLIT_DELIM_CAPTURE);

    $i = 0;
    foreach ($words as $key=>$one) {
        if (!preg_match('/[\s,]+/', $one) && $one){
            $i++;
            $words[$key] = checkWord($i, $one);
        }
    }

    $words = implode('', $words);

    return $words;
}


function checkWord($number, $word) {
    if ($number % 3 == 0 && $number % 5 == 0) {
        return '-ПЯТНАДЦАТЬ-';
    } elseif ($number % 3 == 0) {
        return '-ТРИ-';
    } elseif ($number % 5 == 0) {
        return '-ПЯТЬ-';
    }

    return $word;
}