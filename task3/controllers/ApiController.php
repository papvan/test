<?php

namespace app\controllers;

use app\models\CreditsHistory;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;


class ApiController extends ActiveController
{
    public $modelClass = 'app\models\CreditHistory';

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;
    /**
     * @var string
     */
    public $serializer = 'yii\rest\Serializer';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actionGetCredits()
    {
        return ['res' => ['bla'=>1, 'blabla' => 2]];
    }

    /**
     * @inheritdoc
     */
    public function actionCalculate()
    {
        $credit = new CreditsHistory();
        $credit->attributes = Yii::$app->request->post('CreditHistory');
        $credit->save();
        $credit->generatePayments();
        $credit->savePayments();

        return [$credit->payments];
    }

}