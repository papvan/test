'use strict';

var controllers = angular.module('controllers', []);

controllers.controller('CalculatorController', ['$scope', 'CalculatorService',
    function ($scope, CalculatorService) {
        $scope.loading = false;
        $scope.payments = [];
        $scope.credit = {
            sum: '',
            percent: '',
            dateFirstPay: '',
            month: ''
        };

        $scope.submit = function(){
            if ($scope.CreditForm.$valid){
                $scope.loading = true;
                console.log($scope.credit);

                $scope.credit.dateFirstPay = $scope.CreditForm.date.$viewValue;
                CalculatorService.post({CreditHistory: $scope.credit}).then(function (data) {
                    if (data.status == 200){
                        $scope.payments = data.data[0];
                    }
                    $scope.loading = false;
                }, function (err) {
                    console.log(err);
                })
            }
        };



        // Datepicker init
        $scope.credit.date = new Date();

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        }

        $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };


        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }




    }
]);