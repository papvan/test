'use strict';

var app = angular.module('app');

app.service('CalculatorService', function($http) {
    this.post = function (data) {
        return $http.post('/api/calculate', data);
    };
});
