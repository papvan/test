<?php

use yii\db\Migration;

class m170921_165539_create_credit_history_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('credits_history', [
            'id' => $this->primaryKey(),
            'sum' => $this->integer()->notNull(),
            'percent' => $this->integer()->notNull(),
            'month' => $this->integer()->notNull(),
            'dateFirstPay' => $this->date()->notNull(),
        ]);

        $this->createTable('calculate_history', [
            'id' => $this->primaryKey(),
            'credit_id' => $this->integer()->notNull(),
            'pay_number' => $this->integer()->notNull(),
            'indebtedness' => $this->decimal(24,2)->notNull(),
            'percentages' => $this->decimal(24,2)->notNull(),
            'debt' => $this->decimal(24,2)->notNull(),
            'amount' => $this->decimal(24,2)->notNull(),
            'date' => $this->date()->notNull(),
            'type' => $this->string()->notNull(),
        ]);

        $this->createIndex(
            'idx-calculate-credit_id',
            'calculate_history',
            'credit_id'
        );
    }

    public function safeDown()
    {
        $this->dropTable('credits_history');
        $this->dropTable('calculate_history');
    }
}
