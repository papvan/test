<?php

namespace app\models;


namespace app\models;

use Yii;

/**
 * This is the model class for table "calculate_history".
 *
 * @property integer $id
 * @property integer $credit_id
 * @property integer $pay_number
 * @property string $indebtedness
 * @property string $percentages
 * @property string $debt
 * @property string $amount
 * @property string $date
 * @property string $type
 */
class CalculateHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{calculate_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['credit_id', 'pay_number', 'indebtedness', 'percentages', 'debt', 'amount', 'date', 'type'], 'required'],
            [['credit_id', 'pay_number'], 'integer'],
            [['indebtedness', 'percentages', 'debt', 'amount'], 'number'],
            [['date'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'credit_id' => 'ID кредита',
            'pay_number' => 'Номер платежа',
            'indebtedness' => 'Остаток долга',
            'percentages' => 'Начисленные проценты',
            'debt' => 'Основной долг',
            'amount' => 'Сумма платежа',
            'date' => 'Дата платежа',
            'type' => 'Вид платежа',
        ];
    }

    /**
     * @inheritdoc
     * @return CreditsHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CreditsHistoryQuery(get_called_class());
    }
}