<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "credits_history".
 *
 * @property integer $id
 * @property integer $sum
 * @property integer $percent
 * @property integer $month
 * @property string $dateFirstPay
 */
class CreditsHistory extends \yii\db\ActiveRecord
{
    public $payments;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{credits_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sum', 'percent', 'month', 'dateFirstPay'], 'required'],
            [['sum', 'percent', 'month'], 'integer'],
            [['dateFirstPay'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Сумма кредита',
            'percent' => 'Процент годовых',
            'month' => 'Срок кредита (мес.)',
            'dateFirstPay' => 'Дата первого платежа',
        ];
    }

    /**
     * @inheritdoc
     * @return CreditsHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CreditsHistoryQuery(get_called_class());
    }

    public function generatePayments()
    {
        $i = $this->percent/$this->month/100;
        $payments = [];
        $amount = $this->generateAmount();
        $date = \DateTime::createFromFormat('Y-m-d', $this->dateFirstPay);

        $payments[1]['amount'] = $amount;
        $payments[1]['indebtedness'] = (int)$this->sum;
        $payments[1]['percentages'] = round($this->sum*$i,2, PHP_ROUND_HALF_UP);
        $payments[1]['debt'] = round($payments[1]['amount']-$payments[1]['percentages'], 2,PHP_ROUND_HALF_UP);
        $payments[1]['date'] = $date->format('Y-m-d');
        $payments[1]['type'] = 'аннуитетный';



        for($n = 2; $n <= $this->month; $n++){
            $payments[$n]['amount'] = $amount;
            $payments[$n]['indebtedness'] = round($payments[$n-1]['indebtedness']-$payments[$n-1]['debt'], 2, PHP_ROUND_HALF_UP);
            $payments[$n]['percentages'] = round($payments[$n]['indebtedness']*$i,2, PHP_ROUND_HALF_UP);
            $payments[$n]['debt'] = round($payments[$n]['amount']-$payments[$n]['percentages'], 2, PHP_ROUND_HALF_UP);
            $payments[$n]['date'] = $date->modify('+1 month')->format('Y-m-d');
            $payments[$n]['type'] = 'аннуитетный';
        }

        $this->payments = $payments;
    }

    private function generateAmount()
    {
        $i = $this->percent/$this->month/100;

        return round($this->sum*(($i*pow(1+$i,$this->month))/(pow(1+$i,$this->month)-1)), 2, PHP_ROUND_HALF_UP);
    }

    public function savePayments()
    {
        foreach ($this->payments as $key=>$payment){
            $calculateHistory = new CalculateHistory();
            $calculateHistory->credit_id = $this->id;
            $calculateHistory->pay_number = $key;
            $calculateHistory->attributes = $payment;
            $calculateHistory->save();
        }
    }
}
