<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CalculateHistory]].
 *
 * @see CalculateHistory
 */
class CalculateHistoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return CalculateHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CalculateHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}