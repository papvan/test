<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CreditsHistory]].
 *
 * @see CreditsHistory
 */
class CreditsHistoryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return CreditsHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CreditsHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}