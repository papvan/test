<?php

/* @var $this yii\web\View */

$this->title = 'Кредитный калькулятор';
?>
<div class="site-index" data-ng-controller="CalculatorController">

    <div class="jumbotron">
        <h1>Кредитный калькулятор</h1>

        <p class="lead">Расчет аннуитетного платежа.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-5">
                <h2>Данные по кредиту</h2>

                <form name="CreditForm" ng-submit="submit()" novalidate>
                    <div class="form-group">
                        <label for="sum">Сумма кредита</label>
                        <input type="text" class="form-control" id="CreditFormSum" name="sum" ng-model="credit.sum" required />
<!--                        <input type="text" class="form-control is-invalid" id="validationServer03" placeholder="City" required>-->
<!--                        <div class="invalid-feedback">-->
<!--                            Please provide a valid city.-->
<!--                        </div>-->
                    </div>

                    <div class="form-group">
                        <label for="percent">Процент годовых</label>
                        <div class="input-group mb-2 mb-sm-0">
<!--                            <div class="input-group-addon">%</div>-->
                            <input type="text" class="form-control" id="CreditFormPercent" name="percent" ng-model="credit.percent" required />
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="month">Срок кредита (мес.)</label>
                        <input type="text" class="form-control" id="CreditFormMonth"  name="month" ng-model="credit.month" required />
                    </div>

                    <div class="form-group">
                        <label for="date">Дата первого платежа</label>
                        <p class="input-group">
                            <input type="text" class="form-control" uib-datepicker-popup name="date" ng-model="credit.date" is-open="popup2.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" required />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </p>
                    </div>

                    <button type="submit" class="btn btn-primary" ng-disabled="LovelyForm.$invalid">Расчитать</button>
                </form>
            </div>
            <div class="col-lg-7 align-self-end">
                <h2>Платежи по кредиту</h2>
                <table class="table  table-striped table-bordered table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>№ платежа</th>
                        <th>Остаток долга</th>
                        <th>Начисленные %</th>
                        <th>Основной долг</th>
                        <th>Сумма платежа</th>
                        <th>Дата платежа</th>
                        <th>Вид платежа</th>
                    </tr>
                    </thead>
                    <tbody ng-repeat="(key, payment) in payments">
                    <tr>
                        <th scope="row">{{key}}</th>
                        <td>{{payment.indebtedness | number}}</td>
                        <td>{{payment.percentages | number}}</td>
                        <td>{{payment.debt | number}}</td>
                        <td>{{payment.amount | number}}</td>
                        <td>{{payment.date}}</td>
                        <td>{{payment.type}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
